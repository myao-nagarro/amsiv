import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;

def value = "1619564299000";
long source = Long.valueOf(value);
FileTime time = FileTime.fromMillis(source);

def date = new Date(time.toMillis());
def newDate = new SimpleDateFormat("dd/MMM/yy hh:mm aaa").format(date) 
return newDate