import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
   
/* Get Insight Object Facade from plugin accessor */
Class objectFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade");  
def objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass);
 
/* Get Insight Object Attribute Facade from plugin accessor */
Class objectTypeAttributeFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade"); 
def objectTypeAttributeFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectTypeAttributeFacadeClass);
 
 
/* Get the factory that creates Insight Attributes */
Class objectAttributeBeanFactoryClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.services.model.factory.ObjectAttributeBeanFactory");
def objectAttributeBeanFactory = ComponentAccessor.getOSGiComponentInstanceOfType(objectAttributeBeanFactoryClass);
 
/* This is the custom field with the value you want to add to an object attribute */
CustomField jiraCustomField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(12345);   // Change 12345 to the correct value
 
/* This is the custom field where the object/s you want to set the value */
CustomField insightCustomField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(23456);   // Change 23456 to the correct value
def insightObjects = issue.getCustomFieldValue(insightCustomField);   // "issue" variable is always accessible in post function scripts.
 
/* This is the priority object type attribute and the one we want to modify */
def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttributeBean(8799);
 
if (insightObjects != null) {
    insightObjects.each{insightObject ->
        /* Create the new attribute bean based on the value */
        def newObjectAttributeBean = objectAttributeBeanFactory.createObjectAttributeBeanForObject(insightObject, objectTypeAttributeBean, issue.getCustomFieldValue(jiraCustomField));
        /* Load the attribute bean */
        def objectAttributeBean = objectFacade.loadObjectAttributeBean(insightObject.getId(), objectTypeAttributeBean.getId());
        if (objectAttributeBean != null) {
           /* If attribute exist reuse the old id for the new attribute */
           newObjectAttributeBean.setId(objectAttributeBean.getId());
        }
        /* Store the object attribute into Insight. */
        try {
            objectAttributeBean = objectFacade.storeObjectAttributeBean(newObjectAttributeBean);
        } catch (Exception vie) {
            log.warn("Could not update object attribute due to validation exception:" + vie.getMessage());
        }
    }
}
 
 
 
/* Done! :) */
return true;